- # o̊
- [[j0lms]]
	- [[person]] [[composer]] [[developer]]
	- [[twitter]] https://twitter.com/j0lms
	- [[mastodon]] https://mastodon.social/@j0lms
	- [[bluesky]] https://bsky.app/profile/j0lms.bsky.social
	- [[github]] https://github.com/j0lms
	- [[codeberg]] https://codeberg.org/j0lms
	- [[digital-garden]] https://codeberg.org/j0lms/notes-md/
	- [[telegram]] https://t.me/j0lms
	- [[matrix]] @j0lms:matrix.org
	- [[discord]] j0lms#8968
	- [[soundcloud]] https://soundcloud.com/j0lms
