import os
from selenium import webdriver
from selenium.webdriver.common.by import By

user='j0lms'

def initialize_driver:
    chrome_options=webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver=webdriver.Chrome(options=chrome_options)
    return driver

def get_lichess_data:
    print('head to user profile')
    driver.get('https://lichess.org/@/{}'.format(user))

def get_duolingo_data:
    print('head to user profile')
    driver.get('https://www.duolingo.com/profile/{}'.format(user))
